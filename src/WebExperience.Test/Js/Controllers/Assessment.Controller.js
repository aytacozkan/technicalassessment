﻿assessmentPortalModule.controller("assesmentController", function ($scope, $http, Notification, uiGridConstants, uiGridGroupingConstants) {

    $scope.getDataList = function () {
        $scope.loadingPromise = $http.get("/api/Asset/")
              .then(function (response) {
                  $("#dataGrid")
                  .dxDataGrid({
                      dataSource: response.data,
                      sorting: {
                          mode: 'multiple'
                      },
                      columns: [{
                          dataField: 'CategoryID',
                          caption: 'CategoryID',
                          width: 100
                      }, {
                          dataField: 'CategoryName',
                          caption: 'CategoryName',
                          width: 100
                      }, {
                          dataField: 'Description',
                          caption: 'Description',
                          width: 100
                      }, {
                          caption: "Editing",
                          cellTemplate: function (container, response) {
                              $('<a><button type="button" class="btn btn-info">Edit</button></a>')
                                  .attr('href',
                                      '/api/Asset/' + response.data.Id)
                                  .appendTo(container);

                              $('<button type="button" class="btn btn-danger">Delete</button>')
                                  .on('dxclick',
                                      function () {
                                          $scope.delete(response.data.Id);
                                      })
                                  .appendTo(container);
                          }
                      }],
                      loadPanel: {
                          enabled: false
                      },
                      scrolling: {
                          mode: 'virtual'
                      },

                      summary: {
                          groupItems: []
                      },
                      filterRow: {
                          visible: true,
                          showOperationChooser: true,
                      },
                      groupPanel: {
                          visible: true
                      },
                      width: "100%",
                      allowColumnResizing: true,
                      export: {
                          enabled: true,
                          fileName: 'Hedger Mapping List'
                      }
                  });

              }, function (response) {
                  Notification.error({
                      message: 'Error Occured ' + response.data.ExceptionMessage,
                      positionY: 'buttom',
                      positionX: 'right'
                  });
              });


    }

});