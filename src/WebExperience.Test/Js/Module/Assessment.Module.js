﻿//@author: Aytac Ozkan

var assessmentPortalModule = angular
    .module("assessmentPortal",
    [
    'dx', 'ui-notification', 'ui.grid', 'ui.grid.grouping', 'ui.grid.exporter', 'cgBusy', 'angucomplete-alt'
    ]);


assessmentPortalModule.controller("assesmentPortalController", function ($scope, $http, Notification) {
    $scope.$on('LOAD', function () { $scope.loading = true });
    $scope.$on('UNLOAD', function () { $scope.loading = false });

    $scope.dateFormat = "D.M.YYYY";
    $scope.dateTimeFormat = "D.M.YYYY HH:mm:ss";
    $scope.dateTimeMilliSecondsFormat = "D.M.YYYY HH:mm:ss.SSS";

    $scope.valueDateTimeFormat = 'YYYY-M-D HH:mm:ss';
    $scope.valueDateTimeMilliSecondsFormat = 'YYYY-M-D HH:mm:ss.SSS';
});