﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebExperience.Data.Context;
using WebExperience.Common;
namespace WebExperience.Test.Controllers
{
    public class AssesmentController : Controller
    {
        // GET: Assesment
        public ActionResult Index()
        {

            return View();

        }

        // GET: Assesment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Assesment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Assesment/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Assesment/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Assesment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Assesment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Assesment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
