﻿using System.Web;
using System.Web.Optimization;

namespace WebExperience.Test
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
               .Include("~/Scripts/jquery-{version}.js")
               .Include("~/Scripts/jquery-migrate-{version}.min.js")
               .Include("~/Scripts/jquery.unobtrusive-ajax.js")
               .Include("~/Scripts/jquery.pagination.js")
               );

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular")
            .Include("~/Scripts/angular.min.js")
            .Include("~/Scripts/angular-sanitize.min.js")
            .Include("~/Scripts/angular-ui-notification.min.js")
            .Include("~/Scripts/angular-ui-grid.min.js")
            .Include("~/Scripts/angular-busy.min.js")
            .Include("~/Scripts/angucomplete-alt.js")
            .Include("~/Scripts/angularjs-dropdown-multiselect.js")
            .Include("~/Scripts/ngMask.min.js")
            .Include("~/Scripts/angular-bootstrap-datetimepicker/src/js/datetimepicker.js")
            .Include("~/Scripts/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js")
            .Include("~/Scripts/angular-moment-picker/js/angular-moment-picker.min.js")
            );

            bundles.Add(new StyleBundle("~/bundles/angularStyles")
                .Include("~/Content/angular-ui-notification.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/angular-ui-grid.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/angular-busy.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/angucomplete-alt.css", new CssRewriteUrlTransform())
                .Include("~/Scripts/angular-bootstrap-datetimepicker/src/css/datetimepicker.css", new CssRewriteUrlTransform())
                .Include("~/Scripts/angular-moment-picker/css/angular-moment-picker.min.css", new CssRewriteUrlTransform())
                );

            bundles.Add(new ScriptBundle("~/bundles/angularModules")
                .Include("~/Js/Module/*.js")
                .Include("~/Js/Controllers/*.js"));

            bundles.Add(new StyleBundle("~/bundles/jqueryuiStyles")
           .Include("~/Content/jquery-ui-1.11.2/jquery-ui.css", new CssRewriteUrlTransform())
           .Include("~/Content/jquery-ui-1.11.2/jquery-ui.structure.css", new CssRewriteUrlTransform())
           .Include("~/Content/jquery-ui-1.11.2/jquery-ui.theme.css", new CssRewriteUrlTransform())
           );

            bundles.Add(new ScriptBundle("~/bundles/jqueryuiScripts")
                .Include("~/Content/jquery-ui-1.11.2/jquery-ui.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/devExpress")
               .Include("~/Scripts/jquery.easing.1.3.min.js")
               .Include("~/Scripts/jquery.globalize.min.js")
               .Include("~/Scripts/globalize.min.js")
               .Include("~/Scripts/jquery.globalize/cultures/globalize.culture.en-US.js")
               //.Include("~/Scripts/dx.webappjs.js")
               //.Include("~/Scripts/dx.chartjs.js")
               .Include("~/Scripts/jszip.min.js")
               .Include("~/Scripts/jsrender.min.js")
               .Include("~/Scripts/ej.web.all.min.js")
               );


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
