﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebExperience.Common.Model.Domain;

namespace WebExperience.Data.Context
{
    public class AssessmentContext : DbContext
    {
        public AssessmentContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {

        }
        public AssessmentContext(DbConnection existingConnection, bool contextOwnsConnection) : base(existingConnection, contextOwnsConnection)
        {

        }
        public AssessmentContext(ObjectContext objectContext, bool dbContextOwnsObjectContext) : base(objectContext, dbContextOwnsObjectContext)
        {

        }
        public AssessmentContext(string nameOrConnectionString, DbCompiledModel model) : base(nameOrConnectionString, model)
        {

        }
        public AssessmentContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection) : base(existingConnection, model, contextOwnsConnection)
        {
        }

        public AssessmentContext() : this("DefaultConnection")
        {

        }
        protected AssessmentContext(DbCompiledModel model):base(model)
        {

        }

        public IDbSet<Categories> Categories { get; set; }

    }
}
