﻿using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            //@aytac
            //Scot Hanselman is suggested this image resize plugin, and actually it's easy to use
            //here you go to him blog post[http://www.hanselman.com/blog/NuGetPackageOfTheWeekImageProcessorLightweightImageManipulationInC.aspx]

            //In addition if you want you can use all ImageFormat support by ISupportedImageFormat from plugin.

            OnImageResize(@"D:\aytacozkan.jpg", @"D:\aytac.jpg", 256, 256);

        }


        /// <summary>
        /// To Resize images that specified width and height
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="outPut"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="format"></param>
        public void OnImageResize(string filePath, string outPut, int width, int height)
        {
            // Read a file and resize it.
            byte[] photoBytes = File.ReadAllBytes(filePath);
            int quality = 70;

            Size size = new Size(width, height);

            using (MemoryStream inStream = new MemoryStream(photoBytes))
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    using (ImageFactory imageFactory = new ImageFactory())
                    {

                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Resize(size)
                                    .Quality(quality)
                                    .Save(outPut);
                    }

                    // Do something with the stream.
                    //@aytac
                }
            }
        }
    }


}
