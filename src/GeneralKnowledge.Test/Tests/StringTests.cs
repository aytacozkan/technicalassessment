﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// @aytac
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

            // TODO:
            // Write an algoritm that gets the unique characters of the word below
            // and counts the number of occurrences for each character found

            //@aytac the simple way.
            int occurrences = Regex.Matches(Regex.Escape(word), "x").Count;

            //@aytac, or we can find out which character's counts of occurrences as below;
            string[] source = word.Split(new char[] { '.', '?', '!', ' ', ';', ':', ',' }, StringSplitOptions.RemoveEmptyEntries);

            var wordsAndCount = source.GroupBy(x => x.ToLower()).Select(x => new { Word = x.Key, Count = x.Count() }).OrderByDescending(x => x.Count).ThenBy(x => x.Word);

            Dictionary<string, int> wordCount = new Dictionary<string, int>();

            foreach (var item in wordsAndCount)
            {
                wordCount.Add(item.Word, item.Count);
            }
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO:
            // Write logic to determine whether a is an anagram of b

            if (a.Length != b.Length)
                return false;

            if (a == b)
                return true; //or false don't know whether a string counts as an anagram of itself.

            Dictionary<char, int> pool = new Dictionary<char, int>();
            foreach (char element in a.ToCharArray())//fill the directory with that available chars and count them up
            {
                if (pool.ContainsKey(element))
                    pool[element]++;
                else
                    pool.Add(element, 1);
            }
            foreach (char element in b.ToCharArray())
            {
                if (!pool.ContainsKey(element))
                    return false;
                if (--pool[element] == 0)
                    pool.Remove(element);

            }

            return pool.Count == 0;
        }
    }
}
