﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO:
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            try
            {
                List<Measurement> measurementList = new List<Measurement>();

                var doc = XDocument.Parse(xml);

                foreach (XElement xitem in doc.Descendants("measurement"))
                {
                    Measurement measurement = new Measurement();

                    foreach (var el in xitem.Elements())
                    {

                        if (el.Attribute("name").Value == "temperature")
                        {
                            measurement.Temperature = Convert.ToDecimal(el.Value);
                        }
                        if (el.Attribute("name").Value == "pH")
                        {
                            measurement.Ph = Convert.ToDecimal(el.Value);
                        }
                        if (el.Attribute("name").Value == "Phosphate")
                        {
                            measurement.Phosphate = Convert.ToDecimal(el.Value);
                        }
                        if (el.Attribute("name").Value == "Chloride")
                        {
                            measurement.Chloride = Convert.ToDecimal(el.Value);
                        }
                        if (el.Attribute("name").Value == "Nitrate")
                        {
                            measurement.Nitrate = Convert.ToDecimal(el.Value);
                        }
                    }
                    measurementList.Add(measurement);
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
    public class Measurement
    {
        public decimal Temperature { get; set; } = 0;
        public decimal Phosphate { get; set; } = 0;
        public decimal Ph { get; set; } = 0;
        public decimal Chloride { get; set; } = 0;
        public decimal Nitrate { get; set; } = 0;
    }


}
