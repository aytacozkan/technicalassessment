﻿using CsvHelper;
using GeneralKnowledge.Test.App.helper;
using GeneralKnowledge.Test.App.Model.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            //https://www.nuget.org/packages/CsvHelper/ used @aytac
            var csvFile = Resources.AssetImport;
            try
            {
                List<Asset> assetList = new List<Asset>();
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"D:\stylelabs.interview\src\GeneralKnowledge.Test\Resources\AssetImport.csv"))
                {
                    CsvReader csvread = new CsvReader(file);

                    while (csvread.Read())
                    {
                        int count = csvread.FieldHeaders.Count();

                        Asset asset = new Asset();
                        asset.Id = csvread.GetField<string>("asset id");
                        asset.FileName = csvread.GetField<string>("file_name");
                        asset.Country = csvread.GetField<string>("country");
                        asset.Email = csvread.GetField<string>("email");
                        asset.MimeType = csvread.GetField<string>("mime_type");
                        asset.CreatedBy = csvread.GetField<string>("created_by");
                        asset.Description = csvread.GetField<string>("description");

                        assetList.Add(asset);

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
