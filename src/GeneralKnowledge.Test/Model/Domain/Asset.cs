﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Model.Domain
{
    [Table("asset")]
    public class Asset
    {
        [Key]
        [Column("asset id")]
        [DatabaseGenerated( DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        [Column("file_name")]
        public string FileName { get; set; }
        [Column("mime_type")]
        public string MimeType { get; set; }
        [Column("created_by")]
        public string CreatedBy { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("country")]
        public string Country { get; set; }
        [Column("description")]
        public string Description { get; set; }
    }
}
